<?php
session_start();

use library\Url;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization");
header("Access-Control-Allow-Methods', 'POST, PUT, DELETE, OPTIONS");
header("Content-Type', 'application/json");

function checkFile($className){
    $fileName = 'core/' . str_replace('\\', '/', $className) . '.php';
    return file_exists($fileName);
}

function __autoload($className){
    $fileName = 'core/' . str_replace('\\', '/', $className) . '.php';
    if(!file_exists($fileName)){
        var_dump($fileName);
        throw new Exception('Class not found!', '404');
    }
    require_once $fileName;
}

$controllerName = Url::getSegment(0);

$actionName = Url::getSegment(1);


if(is_null($controllerName)){
    $controller = 'controllers\ControllerMain';
}else {
    $controller = 'controllers\Controller' . ucfirst($controllerName);
}

if(is_null($actionName)){
    $action = "actionIndex";
}else {
    $action = 'action' . ucfirst($actionName);
}

try {
    $fileName = 'core/' . str_replace('\\', '/', $controller) . '.php';
    if(!file_exists($fileName)){
        throw new library\HttpException('Not found!', '404');
    }
    $controller = new $controller();

    if(!method_exists($controller, $action)){
        throw new Exception('Not found!', '404');
    }

    $controller->$action();

}catch(\library\HttpException $e){
    header("HTTP/1.1 " . $e->getCode() . " " . $e->getMessage());
    die("Page not found!");
}catch(Exception $e){
    die($e->getMessage());
}


?>