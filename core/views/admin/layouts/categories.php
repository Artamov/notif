<?php
$categories = $data['categories'];

?>


<html>
<head>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400;300" rel="stylesheet" type="text/css">
    <link href="/assets/css/style.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width">
</head>
<body>

<div class="menu">

    <!-- Иконка меню -->
    <div class="icon-close">
        <img src="/assets/img/close-btn.png">
    </div>

    <!-- Меню -->
    <ul>
        <li><a href="/admin/orders/">Заказы</a></li>
        <li><a href="/admin/allDishes/">Блюда</a></li>
        <li><a href="/admin/slider/">Слайдер</a></li>
        <li><a href="/main/index/" target="_blanc">На сайт</a></li>
        <li><a href="/admin/logout/">Выйти</a></li>
    </ul>
</div>

<!-- Main body -->
<div class="background">

    <div class="icon-menu">
        <img src="/assets/img/menu-ham-icon.png">
        Меню
    </div>
    <div class="centerProducts">
        <div class="addBtn"><h3>Все Категории</h3>
            <form action="/admin/addCategory" method="post">
                <input type="text" name="title">
                <button>Добавить категорию</button>
            </form>
        </div>
        <p>
            <?php for($i = 0; $i <= count($categories)-1; $i++):?>
            <span style="padding: 0 20px;"><?= $categories[$i]['title'];?>&nbsp;&nbsp;<a href="/admin/updateCategoryPriority?id=<?= $categories[$i]['id'];?>">Выводить раньше</a>&nbsp;&nbsp;<a href="/admin/deleteCategory?id=<?= $categories[$i]['id'];?>">Удалить</a></span>
            <?php endfor;?>
        </p>


    </div>

</div>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="/assets/js/menu.js"></script>
</body>
</html>
