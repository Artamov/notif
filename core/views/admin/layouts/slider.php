<?php
	$imgs = $data['slider'];

?>

<html>
  <head>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400;300" rel="stylesheet" type="text/css">
    <link href="/assets/css/style.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width">
  </head>
  <body>

    <div class="menu">
      
      <!-- Иконка меню -->
      <div class="icon-close">
        <img src="/assets/img/close-btn.png">
      </div>

      <!-- Меню -->
     <ul>
          <li><a href="/admin/orders/">Заказы</a></li>
      <li><a href="/admin/allDishes/">Блюда</a></li>
      <li><a href="/admin/slider/">Слайдер</a></li>
      <li><a href="/main/index/" target="_blanc">На сайт</a></li>
      <li><a href="/admin/logout/">Выйти</a></li>
      </ul>
    </div>

    <!-- Main body -->
    <div class="background">

      <div class="icon-menu">
        <img src="/assets/img/menu-ham-icon.png">
        Меню
      </div>
      <div class="sliderWrap">
      	<h2>Слайдер</h2>
			<?php for($i = 0; $i <= count($imgs)-1;$i++):?>
				<div style="margin-top: 20px;border-bottom: 1px solid #000; padding: 10px;">
					<img src="/<?= $imgs[$i]['src']; ?>" width="70%">
          <div><a href="/admin/updatePriorityImg?pr=begin&id=<?= $imgs[$i]['id']; ?>" style="font-size: 20px;">Выводить на 1 позицию вперед</a></div>
          
					<div><a href="/admin/deleteImg?id=<?= $imgs[$i]['id']; ?>" style="font-size: 20px;">Удалить</a></div>
				</div>
			<?php endfor; ?>
			<form action="/admin/addSlider/" method="post" enctype="multipart/form-data">
					<h3>Добавить изображение</h3>
					<div class="file-upload">

     <label>
          <input type="file" name="img">
          <span>Выберите файл</span>
     </label>
</div>
					<input type="submit" value="Добавить" class="saveBtn">

			</form>
      </div>	
    </div>
    
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/assets/js/menu.js"></script>
  </body>
</html>
