<?php

    $order = $data['order'][0];
    $cart = $data['cart'];
    var_export($cart);

?>
<html>
  <head>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400;300" rel="stylesheet" type="text/css">
    <link href="/assets/css/style.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width">
  </head>
  <body>

    <div class="menu">
      
      <!-- Иконка меню -->
      <div class="icon-close">
        <img src="/assets/img/close-btn.png">
      </div>

      <!-- Меню -->
      <ul>
        	<li><a href="/admin/orders/">Заказы</a></li>
			<li><a href="/admin/allDishes/">Блюда</a></li>
			<li><a href="/admin/slider/">Слайдер</a></li>
			<li><a href="/main/index/" target="_blanc">На сайт</a></li>
			<li><a href="/admin/logout/">Выйти</a></li>
      </ul>
    </div>

    <!-- Main body -->
    <div class="background">

      <div class="icon-menu">
        <img src="/assets/img/menu-ham-icon.png">
        Меню
      </div>


    <h1 class="orders-title">Заказ № <?= $order['id']; ?></h1>
	<div class="wrap-order">
		<div class="order-descp">
			<div>Имя клиента:</div>
			<div>Телефон:</div>
			<div>Время доставки:</div>
			<div>Время создания заказа:</div>
			<div>Способ оплаты:</div>
			<div>Улица:</div>
			<div>Номер дома:</div>
			<div>Квартира:</div>
			<div>Подъезд:</div>
			<div>Этаж:</div>
			<div>Название адреса:</div>
			<div>Комментарий:</div>
			<hr>
		</div>
		<div class="order-data">
			<div>|&nbsp;<?= $order['user_name'];?></div>
			<div>|&nbsp;<?= $order['phone'];?></div>
			<div>|&nbsp;<?= $order['time'];?></div>
			<div>|&nbsp;<?= $order['order_gen_date'];?></div>
			<div>|&nbsp;<?= $order['payment_type'];?></div>
			<div>|&nbsp;<?= $order['street'];?></div>
			<div>|&nbsp;<?= $order['home_num'];?></div>
			<div>|&nbsp;<?= $order['kvartira'];?></div>
			<div>|&nbsp;<?= $order['podezd'];?></div>
			<div>|&nbsp;<?= $order['etazh'];?></div>
			<div>|&nbsp;<?= $order['address_type'];?></div>
			<div>|&nbsp;<?= $order['comment'];?></div>
		</div>
		<div class="zakaz">
				Закзанные блюда:
				<ul>
					<?php for($j = 0; $j <= count($cart)-1; $j++): ?>
						<li><?= $cart[$j]['title'];?>&nbsp; | <?= $cart[$j]['pcount'];?>&nbsp;шт | (<?= $cart[$j]['pcount'] ?> * <?= $cart[$j]['pprice'] ?> тг) = <?= $cart[$j]['price']?> тг</li>
					<?php endfor;?>
				</ul>
			</div>
	</div>
    </div>
    
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/assets/js/menu.js"></script>
  </body>
</html>
