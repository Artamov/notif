<?php
	$dish = $data['product'][0];
?>
<html>
  <head>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400;300" rel="stylesheet" type="text/css">
    <link href="/assets/css/style.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width">
  </head>
  <body>

    <div class="menu">
      
      <!-- Иконка меню -->
      <div class="icon-close">
        <img src="/assets/img/close-btn.png">
      </div>

      <!-- Меню -->
    <ul>
          <li><a href="/admin/orders/">Заказы</a></li>
      <li><a href="/admin/allDishes/">Блюда</a></li>
      <li><a href="/admin/slider/">Слайдер</a></li>
      <li><a href="/main/index/" target="_blanc">На сайт</a></li>
      <li><a href="/admin/logout/">Выйти</a></li>
      </ul>
    </div>

    <!-- Main body -->
    <div class="background">

      <div class="icon-menu">
        <img src="/assets/img/menu-ham-icon.png">
        Меню
      </div>
      <div class="dishForm">
      		<h2>Редактировать блюдо</h2>
			<form action="/admin/updateDishes/" method="post" enctype="multipart/form-data">
				<h3>Название блюда</h3>
				<input type="text" name="title" value="<?= $dish['title']; ?>">	
				<h3>Описание блюда</h3>
				<textarea cols="20" rows="10" name="description"><?= trim($dish['description']); ?></textarea>
				<h3>Цена</h3>
				<input type="number" name="price" value="<?= $dish['price']; ?>">
         <h3>Количество(цифра)(необязательно)</h3>
        <input type="number" name="count" value="<?= $dish['count']?>">
                <br><br>
                <span><h3>Вид\Порция\Размер(Главного продукта):<h3> </span><br><input type="text" name="first_type" value="<?= $dish['type']?>">
         <h3>Категория</h3>
        <select name="category" class="cat">
            <?php for($i = 0; $i <= count($data['categories'])-1; $i++):?>
                <option value="<?= $data['categories'][$i]['id'];?>">
                    <?= $data['categories'][$i]['title'];?>
                </option>
            <?php endfor;?>
            <input type="hidden" name="id" value="<?= $dish['id']?>">
        </select>
				<h3>Изображение</h3>
				<img src="/<?= $dish['img']; ?>" width="15%"><br>

				<div class="file-upload">
     <label>
          <input type="file" name="main_photo">
          <span>Выберите файл</span>
     </label>
</div>
				<br><br>
				<input style="display: none;" type="text" value="<?= $dish['id']; ?>" name="pid">
				<input type="submit" value="Сохранить" class="saveBtn">
			</form>
      </div>
    </div>
    
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/assets/js/menu.js"></script>
  </body>
</html>
