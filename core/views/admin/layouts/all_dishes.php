<?php
	$dishes = $data['products'];
?>


<html>
  <head>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400;300" rel="stylesheet" type="text/css">
    <link href="/assets/css/style.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width">
  </head>
  <body>

    <div class="menu">
      
      <!-- Иконка меню -->
      <div class="icon-close">
        <img src="/assets/img/close-btn.png">
      </div>

      <!-- Меню -->
     <ul>
          <li><a href="/admin/orders/">Заказы</a></li>
      <li><a href="/admin/allDishes/">Блюда</a></li>
      <li><a href="/admin/slider/">Слайдер</a></li>
      <li><a href="/main/index/" target="_blanc">На сайт</a></li>
      <li><a href="/admin/logout/">Выйти</a></li>
      </ul>
    </div>

    <!-- Main body -->
    <div class="background">

      <div class="icon-menu">
        <img src="/assets/img/menu-ham-icon.png">
        Меню
      </div>
      <div class="centerProducts">
        <div class="addBtn"><h3>Все блюда</h3><button><a href="/admin/addDishes/">Добавить блюдо</a></button></div>
        
				<?php for($i = 0; $i <= count($dishes)-1;$i++):?>
				<div class="blockProducts">
					<div class="tabOne one">
							<img src="/<?= $dishes[$i]['img']; ?>" alt="images" draggable="false">
					</div>
					
					
					<div class="desc">
						<h3><?= $dishes[$i]['title']; ?></h3>
						<p class="despp">
							<?= $dishes[$i]['description']; ?>
						</p>
            <p class="despp">
              <?= $dishes[$i]['price']; ?> тг
              
              <?php  if($dishes[$i]['count'] != 0){
                echo '<p class="despp">' . $dishes[$i]['count'] . ' шт</p>';
              } ?> 
            
            </p>
					</div>

				  
          <button data-id="<?= $dishes[$i]['id']; ?>" class="updateProduct" style="padding: 5px 10px;"><a href="/admin/updatePriority/?pr=begin&id=<?= $dishes[$i]['id']; ?>" style="color: #000;text-decoration: none;">Выводить на 1 позицию вперед</a></button>
          
					<button data-id="<?= $dishes[$i]['id']; ?>" class="updateProduct" style="padding: 5px 10px;"><a href="/admin/updateDishes/?id=<?= $dishes[$i]['id']; ?>" style="color: #000;text-decoration: none;">Редактировать</a></button>
					<button data-id="<?= $dishes[$i]['id']; ?>" class="deleteDishes" style="padding: 5px 10px;"><a href="/admin/deleteProduct?id=<?= $dishes[$i]['id']; ?>" style="text-decoration: none;color: #000;">Удалить</a></button>
				</div>
				<?php endfor; ?>
		</div>

    </div>
    
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/assets/js/menu.js"></script>
  </body>
</html>
