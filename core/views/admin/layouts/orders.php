<?php
	if(!empty($data['orders']))
	{
		$orders = $data['orders'];
	}
	
?>
<html>
  <head>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400;300" rel="stylesheet" type="text/css">
    <link href="/assets/css/style.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width">
  </head>
  <body>

    <div class="menu">
      
      <!-- Иконка меню -->
      <div class="icon-close">
        <img src="/assets/img/close-btn.png">
      </div>

      <!-- Меню -->
     <ul>
          <li><a href="/admin/orders/">Заказы</a></li>
      <li><a href="/admin/allDishes/">Блюда</a></li>
      <li><a href="/admin/slider/">Слайдер</a></li>
      <li><a href="/main/index/" target="_blanc">На сайт</a></li>
      <li><a href="/admin/logout/">Выйти</a></li>
      </ul>
    </div>

    <!-- Main body -->
    <div class="background">

      <div class="icon-menu">
        <img src="/assets/img/menu-ham-icon.png">
        Меню
      </div>
      <div class="orders">
      	<h1 class="orders-title">Список заказов</h1>

	<table class="table_dark">
	 	 <tr>
      <th>№</th>
	   	<th>Имя клиента</th>
			<th>Телефон</th>
			<th>Время доставки заказа</th>
			<th>Время создания заказа</th>
			<th>Права</th>
	    </tr>
	  	<?php for($i = 0; $i <= count($orders)-1; $i++): ?>
				<tr class="order" data-id="<?= $orders[$i]['id']; ?>"> 
          <td><?= $orders[$i]['id']; ?></td>
					<td><?= $orders[$i]['user_name']; ?></td>
					<td><?= $orders[$i]['phone']; ?></td>
					<td><?= $orders[$i]['time']; ?></td>
					<td><?= $orders[$i]['order_gen_date']; ?></td>
					<td><a href="/admin/deleteOrder?id=<?= $orders[$i]['id'];?>" class="act-del">Удалить</a></td>
				</tr>
		<?php endfor; ?>
  	</table>

      </div>
    </div>
    
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/assets/js/menu.js"></script>
    <script>
		$('.order').click(function(){
			var id = parseInt($(this).attr('data-id'));
			location.href = "/admin/order?id=" + id;
		});
	</script>
  </body>
</html>


