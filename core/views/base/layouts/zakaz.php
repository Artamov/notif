
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="/assets/css/slider/slick.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/slider/slick-theme.css">
	<link rel="stylesheet" href="/assets/css/css.css">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
	<link rel="stylesheet" href="/assets/css/font-awesome.css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="/assets/js/js.js"></script>
</head>
<body>
	<header>
		<div class="modalWindow">
		
	</div>
		<div class="mainHeader www">
			<div class="centerBlock">
				<div class="left">
					<div class="logoblock"><img src="/assets/img/logo.png" alt="logo" class="logo"></div>
				</div>
				<div class="right goto">
					  <h1 class="h1">Sima FooD</h1>
				</div>
			</div>
		</div>
	 
	</header>
	
 	<div class="basketBlockMain">
 		 <p>Оформление заказа</p>
 	</div>
	
	<div class="basketMainZakaz">
		
		<div class="mainBlockCent">
			<!-- <form action="#" method="post"> -->
			<input type="text" placeholder="Имя" id="name" class="goldTwo"><input type="text" placeholder="Телефон" class="goldTwo" id="phone">
			 
			<div class="flex">
				<div class="textFlex">
					<p><i class="fa fa-clock-o" aria-hidden="true"></i>  &nbsp;Заказ на:</p>
				</div>
				<div id='selectBox' style="height: 44px; margin-bottom: 20px;">
				      <p class="valueTag" name="select"></p>
				       <ul id=selectMenuBox>
				       </ul>
				    </div> 
			</div>
			
			<div class="tabsZakaz">
				<div class="tabBlockC">
					<div class="tabOneBlock">
						<p>Доставка</p>
					</div>
					<div class="tabTwoBlock">
						<p>Самовывоз</p>
					</div>
				</div>

				<div class="contentOne">
					<input type="text" placeholder="Улица" class="widthBlockInput" id="ulica"><input type="text" placeholder="Дом" style="width: 100px;" id="dom"> <br>
					<input type="text" id="kvartira" placeholder="Квартира"  class="adresCords">&nbsp;&nbsp;<input type="text" placeholder="Подъезд" id="podezd" class="adresCords">&nbsp;&nbsp;<input type="text" id="kod" placeholder="Код двери" class="adresCords">&nbsp;&nbsp;<input type="text" id="etazh" placeholder="Этаж" class="adresCords">

				<br>

					<textarea name="" id="comment" class="textareaBlock" placeholder="Коментарии"></textarea>

					<!-- <p class="pr">Промокод</p>
					<input type="text" class="disVal" placeholder="Введите промокод" ><button class="btnPromocode">Промокод</button> -->
						
					<div class="oplata">
						<p class="pr">Оплата</p>
						<div class="radioBlock">
								<label>
									<input class="radioTrea" checked type="radio" name="radio-testTwo">
									<span class="radio-customOne"></span>
									<span class="labelOne"> <b class="oplataText">Наличными</b></span>
								</label>
						</div>

						<p class="oplataText">С какой суммы подготовить сдачу?   &nbsp;<input type="text" placeholder="тг" id="sdacha" style="width: 80px; text-align: center; padding-left: 0;"> <input type="checkbox" checked class="checkbox" id="checkbox" /><label for="checkbox">Без сдачи</label></p>
					</div>

				</div>

				<div class="contentTwo">

	                 <label>
	                    <input class="radioTwo" type="radio" name="radio" id="vivoz">
	                    <span class="radio-custom"></span>
	                    <span class="label"><b>Додо Пицца Алтынсарина</b> <br>
	                    	пр-т Алтынсарина, <br>
	                     	 51б 10:00 — 0:00</span>
	                 </label>

				<!-- 	<p class="pr">Промокод</p>
					<input type="text" class="disVal" placeholder="Введите промокод" ><button class="btnPromocode">Промокод</button> -->
						<br><br>
					<div class="oplata">
						<p class="pr">Оплата</p>
						<br>
						<label>
		                    <input class="radioTrea" checked type="radio" name="radio-test">
		                    <span class="radio-customOne"></span>
		                    <span class="labelOne"> <b class="oplataText">Наличными</b></span>
		                 </label>

						<p class="oplataText">С какой суммы подготовить сдачу?   &nbsp;<input type="text" placeholder="тг" style="width: 80px; text-align: center; padding-left: 0;"> <input type="checkbox" checked class="checkboxOne" id="checkboxOne" /><label for="checkboxOne">Без сдачи</label></p>
					</div>
				</div>

			</div>

		<!-- </form>		 -->
		</div>
		
	</div>

	<div class="costBlockMain">
			<h4>Сумма к оплате:	  <span style="color: #ff6900"></span></h4>
	</div>
	
	<div class="btnBlockBasket">
		<a href="/main/basket/"><button class="btnLeft">Назад в корзину</button></a>
		<button class="btnRight" id="order">Оформить</button>
	</div>

	<footer>
		<div class="centerBlockFoooter">
			<p>© 2018</p>
			<p><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i> Facebook</a>&nbsp; <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i> 
			Instagram</a> &nbsp; <a href="#"><i class="fa fa-vk" aria-hidden="true"></i> Вконтакте
			</a></p>
		</div>
	</footer>

	<!-- scripts -->
	
	<script src="/assets/js/selectbox.js"></script>
	<script src="/assets/js/jquery.mask.js"></script>

</body>
</html>