<?php

    $cart = $data['cart'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="/assets/css/slider/slick.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/slider/slick-theme.css">
	<link rel="stylesheet" href="/assets/css/css.css">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
	<link rel="stylesheet" href="/assets/css/font-awesome.css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="/assets/js/js.js"></script>
    <script src="/assets/js/app.js"></script>
</head>
<body>
	<div class="modalWindow">
		
	</div>
	<header>
		<div class="mainHeader www">
			<div class="centerBlock">
				<div class="left">
					<div class="logoblock"><img src="/assets/img/logo.png" alt="logo" class="logo"></div>
				</div>
				<div class="right goto">
					  <h1 class="h1">Sima FooD</h1>
				</div>
			</div>
		</div>
	 
	</header>
	
 	<div class="basketBlockMain">
 		 <p>Корзина</p>
 	</div>
	
	<div class="basketMainZakaz">
			<!-- <div class="defaultText">
				Добавьте что-нибудь из меню
			</div> -->
			
			<!-- Добавленный товар -->
			

			<?php for($i = 0; $i <= count($cart)-1; $i++):?>


			<div class="addProducts">
				
				<div class="addProductsText">

					<h2><?= $cart[$i]['title']; ?></h2>
					<p><?= $cart[$i]['description']; ?></p>
				</div>
                <span class="tg_price" data-price="<?= $cart[$i]['pprice']; ?>" style="display: none"></span>
				<div class="addProductsCount">

					<div class="counter">
					  <button type="button" class="but counterBut dec" data-id="<?= $cart[$i]['pid']; ?>">-</button>
					  <input type="text" class="field fieldCount" value="<?= $cart[$i]['pcount']; ?>" data-min="1">
					  <button type="button" class="but counterBut inc" data-id="<?= $cart[$i]['pid']; ?>">+</button>
					</div>
				</div>

				<div class="addProductsCost">
					<span class="fix_price" style="display:  none;"><?= $cart[$i]['price']; ?></span>
					<p class="tg_price"><?= $cart[$i]['price']; ?></p>
				</div>

				    <div class="close_right_bl_manuTxtInput addProductsRemove " data-id="<?= $cart[$i]['pid']; ?>">
				    	<i class="fa fa-times-circle" aria-hidden="true"></i>
				    </div>


			</div>


			<?php endfor; ?>
			<!-- <div class="addProducts">
				
				<div class="addProductsText">
					<h2>Супермясная (халяль)</h2>
					<p>Традиционное, 25 см</p>
				</div>

				<div class="addProductsCount">
					<div class="counter">
					  <button type="button" class="but counterBut dec">-</button>
					  <input type="text" class="field fieldCount" value="1" data-min="1">
					  <button type="button" class="but counterBut inc">+</button>
					</div>
				</div>

				<div class="addProductsCost">
					<p>2 200 тг.</p>
				</div>
				
				<div class="addProductsRemove">
					<i class="fa fa-times-circle" aria-hidden="true"></i>
				</div>

			</div> -->

			<!-- - - - - - - - - - -->
	</div>

	<div class="costBlockMain">
			<h4>Сумма заказа:  <span style="color: #ff6900" id="all_price_cart">0 тг.</span></h4>
	</div>
	
	<div class="btnBlockBasket">
		<a href="/"><button class="btnLeft">Вернуться в меню</button></a>
		<button class="btnRight" id="goOrder"><a href="/main/order/" style="color: #fff;text-decoration: none;">Заказать</a></button>
	</div>

	<footer>
		<div class="centerBlockFoooter">
			<p>© 2018</p>
			<p><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i> Facebook</a>&nbsp; <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i> 
			Instagram</a> &nbsp; <a href="#"><i class="fa fa-vk" aria-hidden="true"></i> Вконтакте
			</a></p>
		</div>
	</footer>

	<!-- scripts -->

</body>
</html>