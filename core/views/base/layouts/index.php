<?php
    $dishes = $data['products'];
    $categorise = $data['categories'];
    $imgs = $data['slider'];


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="/assets/css/slider/slick.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/slider/slick-theme.css">
	<link rel="stylesheet" href="/assets/css/css.css">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
	<link rel="stylesheet" href="/assets/css/font-awesome.css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="/assets/js/js.js"></script>
    <script src="/assets/js/app.js"></script>
</head>
<body>
	<div class="modalWindow">
		
	</div>
	<header>
		<div class="mainHeader">
			<div class="centerBlock">
				<div class="left">
					<div class="logoblock"><img src="/assets/img/logo.png" alt="logo" class="logo"></div>
				</div>
				<div class="right">
					<div class="BlockTop">
						<div class="blockOne">
							<h1>Sima FooD</h1>
						</div>
						<div class="blockTwo">
							<p>Доставка еды Актобе</p>
							<p><b style="color: #929292">ежедневно с 10:00 до 01:00</b></p>
						</div>
						<div class="blockThrea">
							<p><i class="fa fa-whatsapp whatsapp" aria-hidden="true"></i> <i class="fa fa-telegram telegram" aria-hidden="true"></i> <i class="fa fa-phone phone" aria-hidden="true"></i>&nbsp; 8 (777) 52-000-52</p>
						</div>
					</div>

					<div class="BlockBottom">
						<div class="menu">
							<ul>
                                <?php for($a = 0; $a <= count($categorise)-1; $a++):?>
                                    <li><a href="#<?= $categorise[$a]['title'];?>" class="ancor"><?= $categorise[$a]['title'];?></a></li>
                                <? endfor;?>

								<li><a href="/main/cat/?n=6"">Акции</a></li>
								<li><a href="#">Контакты</a></li>
								<li><a href="#">О нас</a></li>
							</ul>
						</div>
						
						<div class="basket">
							<div class="textBasket">

								<p><span id="products_price">0<?= $price; ?></span> тг.</p>
								<p style="color: #929292" class="cnt_p"><span id="products_count"><?= $count; ?></span>  <i class="fa fa-arrow-up arrw" aria-hidden="true"></i> </p>
								<div class="manuTxtInput">
									<div class="flex_block_main_manu">
										
										<div class="scrooolllll">

                                        </div>


									</div>
								
								</div>
							</div>
							<div class="button">
								<a href="/main/basket/" class="b1">
									<button>
										Корзина
									</button>
								</a>
								<a href="/main/basket/" class="b2">
									<button>
										<i class="fa fa-shopping-basket" aria-hidden="true"></i>
									</button>
								</a>
							</div>
						</div>	

						<div class="toggleBtn">
							<i class="fa fa-bars" aria-hidden="true"></i>
						</div>

						<div class="menuTwoMobile">
							<ul>
								<li><a href="/main/cat/?n=1">Пиццы</a></li>
								<li><a href="/main/cat/?n=2">Комбо</a></li>
								<li><a href="/main/cat/?n=3">Закуски</a></li>
								<li><a href="/main/cat/?n=4">Десерты</a></li>
								<li><a href="/main/cat/?n=5">Напитки</a></li>
								<li><a href="/main/cat/?n=6">Акции</a></li>
								<li><a href="#">Контакты</a></li>
								<li><a href="#">О нас</a></li>
							</ul>
						</div>

					
					</div>
				</div>
			</div>
		</div>
		<!-- slider -->

	
		<section class="lazy slider" data-sizes="50vw">

            <?php for($t = 0; $t <= count($imgs)-1; $t++):?>
                    <div>
                        <img data-lazy="/<?= $imgs[$t]['src'];?>" data-srcset="/<?= $imgs[$t]['src'];?>">
                    </div>
            <?php endfor;?>
            </section>
        </header>

        <div class="nameBeverages">
            <div class="centerBlockTwo">
                 <h2><?= $title;?></h2>
            </div>
        </div>
    <?php for($y = 0; $y <=count($categorise)-1; $y++ ):?>
        <h1 id="<?= $categorise[$y]['title'];?>" style="width: 960px;margin: 0 auto;padding: 10px 0 40px 0;"><?= $categorise[$y]['title'];?></h1>
        <div class="products">
            <div class="centerProducts">
                <h1><?= $notice;?></h1>
                    <?php for($i = 0; $i <= count($dishes)-1;$i++):?>
                     <?php if($dishes[$i]['category'] == $categorise[$y]['id']):?>

                        <div class="tabs blockProducts">
                            <div class="tabs__c active">
                                <div class="tabOne">
                                    <img src="/<?= $dishes[$i]['img']; ?>" alt="images" draggable="false">
                                </div>
                            </div>

                            <?php for($j = 0; $j <= count($dishes[$i]['images'])-1; $j++):?>

                                <div class="tabs__c">
                                    <div class="tabOne">
                                        <img src="/<?= $dishes[$i]['images'][$j]['src']; ?>" alt="images" draggable="false">
                                    </div>
                                </div>
                            <?php endfor;?>


                              <div class="desc">
                            <span>&nbsp;</span>		<h3><?= $dishes[$i]['title']; ?></h3>
                                    <p>
                                        <?= $dishes[$i]['description']; ?>
                                    </p>
                                    <?php if(!empty($dishes[$i]['count'])):?>
                                    	<div style="color: orange;font-size: 18px;">Количество:
                                    		<span ><?= $dishes[$i]['count']; ?></span>&nbsp;шт
                                    	</div>
                                    <?php endif; ?>
                                    
                                </div>
                              <ul class="tabs__caption tabBlock">
                                  <?php if($dishes[$i]['type'] == 'Стандарт'):?>
                                  
                                  <?php endif;?>
                                  <?php if($dishes[$i]['type'] != 'Стандарт'):?>
                                  <li>
                                      <div class="oneTab">
                                          <p><?= $dishes[$i]['type'];?></p>
                                      </div>
                                  </li>


                                      <?php for($n = 0; $n <= count($dishes[$i]['images'])-1; $n++):?>
                                          <li>
                                              <div class="oneTab">
                                                  <p><?= $dishes[$i]['images'][$n]['ptype'];?></p>
                                              </div>
                                          </li>
                                      <?php endfor; ?>
                                  <?php endif; ?>

                              </ul>
                            <div class="tabs__content active">
                                <div class="tabOneCost active">
                                    <p><span class="product_price"><?= $dishes[$i]['price']; ?></span> тг.</p>
                                    <button class="inBasket addToCart" data-id="<?= $dishes[$i]['id']; ?>">В корзину</button>
                                </div>
                            </div>
                            <?php for($g = 0; $g <= count($dishes[$i]['images'])-1; $g++):?>
                                <div class="tabs__content">
                                    <div class="tabOneCost">
                                        <p><span class="product_price"><?= $dishes[$i]['images'][$g]['price']; ?></span> тг.</p>
                                        <button class="inBasket addToCart" data-id="<?= $dishes[$i]['id']; ?>">В корзину</button>
                                    </div>
                                </div>
                            <?php endfor; ?>

                    </div>
            <?php endif;?>
                    <?php endfor; ?>
		</div>
	</div>


    <?php endfor; ?>
	
	<div class="nameBeverages">
		<div class="centerBlockTwo">
			 <h2><?= $napitki;?></h2>
		</div>
	</div>
	
	<div class="beverages">
		<?php for($a = 0; $a <= count($napitki_arr)-1; $a++):?>
		<div class="beveragesBlock">
				<div class="beveragesDesc">
					<p><?= $napitki_arr[$a]['title'];?></p>
				</div>	
				<div class="beveragesImg">
					<img src="/<?= $napitki_arr[$a]['img'];?>" alt="images">
				</div>
				<div class="beveragesCost">
					<p><span class="napprice"><?= $napitki_arr[$a]['price'];?></span>&nbsp; тг.</p>
				</div>
				<div class="beveragesBtn">
					<span class="product_price" style="display: none;"><?= $napitki_arr[$a]['price'];?></span>
					<button class="addToCart" data-id="<?= $napitki_arr[$a]['id'];?>">В корзину</button>
				</div>
		</div>
		<?php endfor; ?>

	</div>
	

	<footer>
		<div class="centerBlockFoooter">
			<p>© 2018</p>
			<p><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i> Facebook</a>&nbsp; <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i> 
			Instagram</a> &nbsp; <a href="#"><i class="fa fa-vk" aria-hidden="true"></i> Вконтакте
			</a></p>
		</div>
	</footer>

	<!-- scripts -->

 	<script src="/assets/css/slider/slick.js" type="text/javascript" charset="utf-8"></script>
    <script>
        $(".lazy").slick({
            lazyLoad: 'ondemand',
            infinite: true,
            fade: true,
            dots: true,
            autoplay: true,
            autoplaySpeed: 4000
        });
    </script>
</body>
</html>