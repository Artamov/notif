<?php
/**
 * Created by PhpStorm.
 * User: Fox
 * Date: 21.07.2018
 * Time: 20:26
 */

namespace models;


use configs\Config;

class DB
{
    private static $_instance = null;


    private function __construct () {
        $name = getenv('DB_NAME');
        $pass = getenv('DB_PASSWORD');
        $user = getenv('DB_USER');
        $host = getenv('DB_HOST');

        $this->_instance = new \PDO(
            'mysql:host=' . $host. ';dbname=' . $name,
           $user,
            $pass,
            [
                \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8mb4'", // установить кодировку
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC, // возвращать ассоциативные массивы
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION // возвращать Exception в случае ошибки
            ]
        );


    }

    // запрещаем клонирование
    private function __clone () {}
    private function __wakeup () {}

    // получаем ссылку
    public static function getInstance()
    {
        if (self::$_instance != null) {
            return self::$_instance;
        }

        return new self;
    }

    // простой запрос
    public function Query($query, $params = [])
    {
        $res = $this->_instance->prepare($query);
        $res->execute($params);
        if($this->_instance->lastInsertId())
            return $this->_instance->lastInsertId();
        return $res;
    }

    // запрос на выборку
    public function Select($query, $params = [])
    {
        $result = $this->Query($query, $params);
        if ($result) {
            return $result->fetchAll();
        }
    }
}