<?php
/**
 * Created by PhpStorm.
 * User: Fox
 * Date: 20.07.2018
 * Time: 10:26
 */

namespace library;


class View
{
    public $basePath = __DIR__ . '/../views/base/templates/';
    protected $title;
    protected $seo = [];
    protected $css = [];
    protected $js = [];

    protected $_layout;


    public function setLayout($type, $layout){
        $this->_layout = __DIR__ . '/../views/' . $type .'/layouts/' . $layout . '.php';
    }

    public function render($data){
        include $this->_layout;
    }

    public function setTitle($str){
        $this->title = $str;
    }
    public function setCss($css){
        $this->css[] = $css;
    }
}