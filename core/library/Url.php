<?php
/**
 * Created by PhpStorm.
 * User: Fox
 * Date: 20.07.2018
 * Time: 10:19
 */

namespace library;


class Url
{
    protected static function getSegmentsFromUrl(){
        $segments = explode('/', $_GET['url']);
        if(empty($segments[count($segments)-1])){
            unset($segments[count($segments)-1]);
        }
        $segments = array_map(function($v){
            return preg_replace('/[\'\\\*]/', '', $v);
        }, $segments);
        return $segments;
    }
    public static function getParam($paramName){
        return addslashes($_GET[$paramName]);
    }

    public static function getSegment($n){
        $segments = self::getSegmentsFromUrl();
        return $segments[$n];
    }
    public static function getAllSegments(){
        return self::getSegmentsFromUrl();
    }
}