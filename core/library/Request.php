<?php
/**
 * Created by PhpStorm.
 * User: Fox
 * Date: 21.07.2018
 * Time: 21:20
 */

namespace library;


class Request
{
    public static $data;

    public static function post()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            self::$data = $_POST;
            return true;
        }
    }
    public static function get()
    {
        if($_SERVER['REQUEST_METHOD'] == 'GET')
        {
            self::$data = $_GET;
            return true;
        }
    }
    public static function put()
    {
        if($_SERVER['REQUEST_METHOD'] == 'PUT')
            return true;
    }
    public static function delete()
    {
        if($_SERVER['REQUEST_METHOD'] == 'DELETE')
            return true;
    }

}