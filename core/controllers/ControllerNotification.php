<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 015 15.09.18
 * Time: 16:54
 */

namespace controllers;


use base\Controller;
use configs\Config;
use library\JWT;
use models\DB;


class ControllerNotification extends Controller
{
    public function actionIndex()
    {
        //var_dump($_POST);
    }
    public function actionAdd()
    {
        $key = getenv('JWT_SECRET');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization");
        header("Access-Control-Allow-Methods', 'POST, PUT, DELETE, OPTIONS");
        header("Content-Type', 'application/json");
        $handler = fopen('php://input', 'r');
        $data = json_decode(stream_get_contents($handler), true);
        if(!empty($data['token'])){
            $title = $data['title'];
            if(empty($title)){
                echo json_encode(['errCode' => 111, 'errMess' => 'Empty title field']);
                exit;
            }
            try {
               $token = JWT::decode($data['token'], $key, ['HS256']);
               $id = $token->{'id'};
               if(DB::getInstance()->Query('INSERT INTO notification (user_id, title) VALUES(:user_id, :title)',
                   [
                       'user_id' => $id,
                       'title' => $title
                   ])){
                   echo json_encode(['status' => "notification success added"]);
               }
            }catch(\Exception $e){
                $err = [
                    'errMess' => $e->getMessage()
                ];
                echo json_encode($err);
            }

        }else {
            echo json_encode(['errCode' => 112, 'errMessage' => 'Empty token!']);
        }


        //var_dump($data);
    }

    public function actionCheck()
    {
        $key = getenv('JWT_SECRET');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization");
        header("Access-Control-Allow-Methods', 'POST, PUT, DELETE, OPTIONS");
        header("Content-Type', 'application/json");
        $token = $_POST['token'];
        if(!empty($token)){
            try{
                $token = JWT::decode($token, $key, ['HS256']);
                if(!empty($token)){
                    $user_id = $token->{'id'};
                    $data = DB::getInstance()->Select('SELECT * FROM notification WHERE user_id = :id',
                        [
                            'id' => $user_id
                        ]);
                    if(!empty($data)){
                        echo json_encode($data);
                    }
                    else {
                        echo json_encode(['status' => 'no notif row']);
                    }
                }
            }catch(\Exception $e){
                echo json_encode(['errMess' => 'invalid token']);
            }
        }else {
            echo json_encode(['errMess' => 'empty token']);
        }
    }
    public function actionDelete()
    {
        $key = getenv('JWT_SECRET');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization");
        header("Access-Control-Allow-Methods', 'POST, PUT, DELETE, OPTIONS");
        header("Content-Type', 'application/json");
        $token = $_POST['token'];
        if(!empty($_POST['id'])){
            $notif_id = $_POST['id'];
        }else {
            $notif_id = 0;
        }
        if(!empty($token)){

            try{

                $token = JWT::decode($token, $key, ['HS256']);
                if(!empty($token)){

                    $user_id = $token->{'id'};
                    $rows = DB::getInstance()->Query('SELECT * FROM notification WHERE id= :id AND user_id= :user_id',
                        [
                            'id' => $notif_id,
                            'user_id' => $user_id
                        ]);

                    if($rows->rowCount() != 0){

                        if(DB::getInstance()->Query('DELETE FROM notification WHERE id = :id',
                            [
                                'id' => $notif_id
                            ])){

                            echo "notification deleted";
                        }else {
                            echo json_encode(['status' => 'fatal error']);
                        }
                    }else {
                        echo json_encode(['status' => 'no rows found']);
                    }

                }
            }catch(\Exception $e){
                echo json_encode(['errMess' => 'invalid token']);
            }
        }else {
            echo json_encode(['errMess' => 'empty token']);
        }
    }
}