<?php
/**
 * Created by PhpStorm.
 * User: Fox
 * Date: 20.07.2018
 * Time: 10:27
 */

namespace base;
use library\View;

abstract class Controller
{
    public function __construct(){
        $this->_view = new View();
        $this->_view->setLayout('base', 'index');
    }
    abstract public function actionIndex();
}