<?php
/**
 * Created by PhpStorm.
 * User: Fox
 * Date: 21.07.2018
 * Time: 22:48
 */

namespace configs;


class Config
{

    public static $db_config = [
        'db_host' => 'localhost',
        'db_user' => 'root',
        'db_password' => '',
        'db_name' => 'notif'
    ];

    public static $token_key = "@@@123456@@@";
    public static function getConfig()
    {
        $appEnv = getenv('APP_ENV');

        if (in_array($appEnv, ['stage','production'])) {
            self::$db_config = [
                'db_host' => getenv('DB_HOST'),
                'db_user' => getenv('DB_USER'),
                'db_password' => getenv('DB_PASSWORD'),
                'db_name' => getenv('DB_NAME')
            ];
            self::$token_key = getenv('JWT_SECRET');
        }
    }

}